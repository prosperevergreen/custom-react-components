import React from "react";

/**
 * A react functional component that makes styling easy
 *
 * @param {{
 * children: object;
 * as: string;
 * color: string;
 * bg: string;
 * d: string;
 * p: string | number
 * pr: string | number;
 * pt: string | number;
 * pl: string | number;
 * pb: string | number;
 * px: string | number;
 * py: string | number;
 * m: string | number;
 * ml: string | number;
 * mr: string | number;
 * mt: string | number;
 * mb: string | number;
 * mx: string | number;
 * my: string | number;
 * w: string;
 * maxW: string;
 * minW: string;
 * h: string;
 * minH: string;
 * maxH: string;
 * justify: string;
 * align: string;
 * direction: string;
 * grow: string | number;
 * shrink: string | number;
 * b: string;
 * br: string;
 * bw: string;
 * bc: string;
 * bs: string;
 * sizing: string;
 * shadow: string;
 * position: string;
 * left: string;
 * right: string;
 * top: string;
 * bottom: string;
 * overflow: string;
 * overflowX: string;
 * overflowY: string;
 * decoration: string;
 * txtAlign: string;
 * txtOverflow: string;
 * transform: string;
 * }} props - the props sent from the parents element
 * @param {object} props.children - react children props
 * @param {string} props.as - the element type/tagname to be constructed. Default is 'div'
 * @param {string} props.color - the text-color for the elements
 * @param {string} props.bg - the background-color for the elements
 * @param {string} props.d - the display type for the elements
 * @param {number | string} props.p - the padding for the elements => p * spacing (spacing = 8px)
 * @param {number | string} props.pr - the padding-right for the elements => pr * spacing (spacing = 8px)
 * @param {number | string} props.pl - the padding-left for the elements => pl * spacing (spacing = 8px)
 * @param {number | string} props.px - the padding-left and padding-right for the elements => pl * spacing (spacing = 8px) && pr * spacing (spacing = 8px)
 * @param {number | string} props.pt - the padding-top for the elements => pt * spacing (spacing = 8px)
 * @param {number | string} props.pb - the padding-bottom for the elements => pb * spacing (spacing = 8px)
 * @param {number | string} props.py - the padding-top and padding-bottom for the elements => pt * spacing (spacing = 8px) && pb * spacing (spacing = 8px)
 * @param {number | string} props.m - the margin for the elements => m * spacing (spacing = 8px)
 * @param {number | string} props.mr - the margin-right for the elements => mr * spacing (spacing = 8px)
 * @param {number | string} props.ml - the margin-left for the elements => ml * spacing (spacing = 8px)
 * @param {number | string} props.mx - the margin-left and margin-right for the elements => ml * spacing (spacing = 8px) && mr * spacing (spacing = 8px)
 * @param {number | string} props.mt - the margin-top for the elements => mt * spacing (spacing = 8px)
 * @param {number | string} props.mb - the margin-bottom for the elements => mb * spacing (spacing = 8px)
 * @param {number | string} props.my - the margin-top and margin-bottom for the elements => mt * spacing (spacing = 8px) && mb * spacing (spacing = 8px)
 * @param {string} props.w - the width for the element
 * @param {string} props.minW - the min-width for the element
 * @param {string} props.maxW - the max-width for the element
 * @param {string} props.h - the height for the element
 * @param {string} props.minH - the min-height for the element
 * @param {string} props.direction - the flex-direction for the element
 * @param {string} props.justify - the justify-content for the element
 * @param {string} props.align - the align-items for the element
 * @param {string|number} props.grow - the flex-grow for the element
 * @param {string} props.shrink - the flex-shrink for the element
 * @param {string} props.b - the border property for the element
 * @param {string} props.bc - the border-colo propertyr for the element
 * @param {string} props.br - the border-radius property for the element
 * @param {string} props.bw - the border-width property for the element
 * @param {string} props.bs - the border-style property for the element
 * @param {string} props.sizing - the box-sizing property for the element
 * @param {string} props.shadow - the box-shadow property for the element
 * @param {string} props.position- the position property for the element
 * @param {string} props.left - the position/distance from the left for the element
 * @param {string} props.right - the position/distance from the right for the element
 * @param {string} props.bottom - the position/distance from the bottom for the element
 * @param {string} props.top - the position/distance from the top for the element
 * @param {string} props.overflow - the overflow property for the element
 * @param {string} props.overflowX - the overflow-x property for the element
 * @param {string} props.overflowY - the overflow-y property for the element
 * @param {string} props.decoration - the text-decoration property for the element
 * @param {string} props.txtAlign - the text-align property for the element
 * @param {string} props.txtOverflow - the text-overflow property for the element
 * @param {string} props.transform - the text-transform property for the element
 * @param {number} props.spacing - the base spacing for all other sizing properties property for the element
 *
 * @returns {object} react element created to with the provided styles
 */
const El = ({
	children,
	as,
	color,
	bg,
	d,
	p,
	px,
	py,
	pr,
	pl,
	pt,
	pb,
	m,
	mx,
	my,
	mt,
	mb,
	mr,
	ml,
	w,
	maxW,
	minW,
	h,
	maxH,
	minH,
	justify,
	align,
	direction,
	grow,
	shrink,
	b,
	br,
	bw,
	bc,
	bs,
	sizing,
	shadow,
	position,
	left,
	right,
	top,
	bottom,
	overflow,
	overflowX,
	overflowY,
	txtAlign,
	decoration,
	txtOverflow,
	transform,
	spacing,
	...otherProps
}) => {
	//  Set the tag name for the element
	const type = as && typeof as === "string" && as !== "" && isValid(as) ? as : "div";

	// Set properties that depend on other properties
	if (px) pl = pr = px;
	if (py) pt = pb = py;
	if (mx) ml = mr = mx;
	if (my) mt = mb = my;

	const isValid = (input) =>
		document.createElement(input).toString() !== "[object HTMLUnknownElement]";

	// Base spacing
	const baseSpacing = spacing ? spacing : 8;

	// Set the relative value with base spacing
	const withSpacing = (value) => `${value * baseSpacing}px`;

	// Create style object based on provided props
	const style = {
		...(color && { color }),
		...(bg && { backgroundColor: bg }),
		...(d && { diplay: d }),
		...(w && { width: w }),
		...(maxW && { maxWidth: maxW }),
		...(minW && { minWidth: minW }),
		...(h && { height: h }),
		...(maxH && { maxHeight: maxH }),
		...(minH && { minHeight: minH }),
		...(p && { padding: isNaN(p) ? p : withSpacing(p) }),
		...(pr && { paddingRight: isNaN(pr) ? pr : withSpacing(pr) }),
		...(pl && { paddingLeft: isNaN(pl) ? pl : withSpacing(pl) }),
		...(pt && { paddingTop: isNaN(pt) ? pt : withSpacing(pt) }),
		...(pb && { paddingBottom: isNaN(pb) ? pb : withSpacing(pb) }),
		...(m && { margin: isNaN(m) ? m : withSpacing(m) }),
		...(mr && { marginRight: isNaN(mr) ? mr : withSpacing(mr) }),
		...(ml && { marginLeft: isNaN(ml) ? ml : withSpacing(ml) }),
		...(mt && { marginTop: isNaN(mt) ? mt : withSpacing(mt) }),
		...(mb && { marginBottom: isNaN(mb) ? mb : withSpacing(mb) }),
		...(justify && { justifyContent: justify }),
		...(align && { alignItems: align }),
		...(direction && { flexDirection: direction }),
		...(grow && { flexGrow: `${grow}` }),
		...(shrink && { flexShrink: `${shrink}` }),
		...(b && { border: b }),
		...(br && { borderRadius: isNaN(br) ? br : withSpacing(br) }),
		...(bc && { borderColor: bc }),
		...(bw && { borderWidth: isNaN(pw) ? pw : withSpacing(bw) }),
		...(bs && { borderStyle: bs }),
		...(sizing && { boxSizing: sizing }),
		...(shadow && { boxShadow: shadow }),
		...(position && { position }),
		...(left && { left }),
		...(right && { right }),
		...(top && { top }),
		...(bottom && { bottom }),
		...(overflow && { overflow }),
		...(overflowX && { overflowX }),
		...(overflowY && { overflowY }),
		...(decoration && { textDecoration: decoration }),
		...(txtAlign && { textAlign: txtAlign }),
		...(txtOverflow && { textOverflow: txtOverflow }),
		...(transform && { textTransform: transform }),
	};

	// Create react element based on provided style and element type/tagname
	return React.createElement(type, { ...otherProps, style }, children);
};

export default El;
