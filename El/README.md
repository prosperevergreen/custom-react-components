# El

[El.js](El.js) exports a **light-weight** simple component called **El** i.e a short form for element. **El** component or `<El></El>` is a functional component that is intended to make _styling_ easy.

# Usage

- By default, `<El>text</El>` will be converted to `<div>text</div>`. **El** component accepts several props to customize the element.

- `{ string } as` props: this props accepts a **string** and is used to specify the element type or tagname. For example, `<El as="h1">text</El>` will produce `<h1>text</h1>`

- `{ string } color` props: this props accepts a **string** and is used for setting the element's text color. For example, `<El color="white" >text</El>` will produce `<div style="color: white;">text</div>`

# Full list of props

- `{ string } as` - the element type/tagname to be constructed. Default is 'div'
- `{ string } color` - the text-color for the elements
- `{ string } bg` - the background-color for the elements
- `{ string } d` - the display type for the elements
- `{ number } spacing` - the base spacing for all other sizing properties property for the element
- `{ string | number } p` - the padding for the elements => p \* spacing (spacing = 8px)
- `{ string | number } pr` - the padding-right for the elements => pr \* spacing (spacing = 8px)
- `{ string | number } pl` - the padding-left for the elements => pl \* spacing (spacing = 8px)
- `{ string | number } px` - the padding-left and padding-right for the elements => pl _ spacing (spacing = 8px) && pr _ spacing (spacing = 8px)
- `{ string | number } pt` - the padding-top for the elements => pt \* spacing (spacing = 8px)
- `{ string | number } pb` - the padding-bottom for the elements => pb \* spacing (spacing = 8px)
- `{ string | number } py` - the padding-top and padding-bottom for the elements => pt _ spacing (spacing = 8px) && pb _ spacing (spacing = 8px)
- `{ string | number } m` - the margin for the elements => m \* spacing (spacing = 8px)
- `{ string | number } mr` - the margin-right for the elements => mr \* spacing (spacing = 8px)
- `{ string | number } ml` - the margin-left for the elements => ml \* spacing (spacing = 8px)
- `{ string | number } mx` - the margin-left and margin-right for the elements => ml _ spacing (spacing = 8px) && mr _ spacing (spacing = 8px)
- `{ string | number } mt` - the margin-top for the elements => mt \* spacing (spacing = 8px)
- `{ string | number } mb` - the margin-bottom for the elements => mb \* spacing (spacing = 8px)
- `{ string | number } my` - the margin-top and margin-bottom for the elements => mt _ spacing (spacing = 8px) && mb _ spacing (spacing = 8px)
- `{ string } w` - the width for the element
- `{ string } minW` - the min-width for the element
- `{ string } maxW` - the max-width for the element
- `{ string } h` - the height for the element
- `{ string } minH` - the min-height for the element
- `{ string } direction` - the flex-direction for the element
- `{ string } justify` - the justify-content for the element
- `{ string } align` - the align-items for the element
- `{ string | number } grow` - the flex-grow for the element
- `{ string } shrink` - the flex-shrink for the element
- `{ string } b` - the border property for the element
- `{ string } bc` - the border-colo propertyr for the element
- `{ string } br` - the border-radius property for the element
- `{ string } bw` - the border-width property for the element
- `{ string } bs` - the border-style property for the element
- `{ string } sizing` - the box-sizing property for the element
- `{ string } shadow` - the box-shadow property for the element
- `{ string } position` - the position property for the element
- `{ string } left` - the position/distance from the left for the element
- `{ string } right` - the position/distance from the right for the element
- `{ string } bottom` - the position/distance from the bottom for the element
- `{ string } top` - the position/distance from the top for the element
- `{ string } overflow` - the overflow property for the element
- `{ string } overflowX` - the overflow-x property for the element
- `{ string } overflowY` - the overflow-y property for the element
- `{ string } decoration` - the text-decoration property for the element
- `{ string } txtAlign` - the text-align property for the element
- `{ string } txtOverflow` - the text-overflow property for the element
- `{ string } transform` - the text-transform property for the element

**Note: Any combination of the props are allowed.**

# Usage

To use the component in your project:

1.  Download the [El.js](./El.zip) component and copy it to the `./src` folder of your React app.

2.  Import the file into your project.

```javascript
import El from "relative/path/to/the/file";
```

3.  Use the component as you like.
