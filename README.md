# My Custom Components

I wrote some custom components to help me work faster with react jsx syntax.

## El

This is a **light-weight** simple component that I would like to call **El** i.e a short form for element. **El** component or `<El></El>` is a functional component that is intended to make *styling* easy.  
For more on **El** component, [README.md](El/README.md).
