import React, { useReducer, createContext, useContext } from "react";

/**
 * A function that creates provider state for context provider value
 *
 * @param {{[string]:[any, function, object:{[string]: object | function }]}} combine Combined state object
 * @returns
 */
function createProviderState(combine) {
	// Get keys of the combine reducer
	const keys = Object.keys(combine);
	// switch reducer function for dispatch function useing useReducer
	const providerState = keys.reduce((acc, key) => {
		const [initState, stateReducer, stateAction] = combine[key];
		const [state, dispatch] = useReducer(stateReducer, initState);
		return {
			...acc,
			[key]: [state, dispatch, stateAction],
		};
	}, {});
	return providerState;
}

// Create context for storing app state
export const StoreContext = createContext({});
// Set context name
StoreContext.displayName = "AppStore";

/**
 * A function that returns the store.
 *
 * Note: The function prevents the need to regularly import useContext to access context value
 *
 * @returns Current store state
 */
export const useStore = () => useContext(StoreContext);

/**
 * A component that provides a global store to it's children component.
 *
 * @param {*} param0 Nested children of component
 * @returns
 */
export const StoreProvider = ({ initStore, children }) => {
	const state = createProviderState(initStore);
	return (
		<StoreContext.Provider value={state}>{children}</StoreContext.Provider>
	);
};