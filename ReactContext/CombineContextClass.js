import React, { useReducer, createContext, useContext } from 'react';

export default class CombineContext {
  /**
   *
   * @param {string} displayName - Display name of the store for debugging
   * @param {any} initValue - Value to initialize the store with
   */
  constructor(displayName = 'Combine Context', initValue = {}) {
    this.context = createContext(initValue);
    this.context.displayName = displayName;
  }

  /**
   * A function that returns the store.
   *
   * Note: The function prevents the need to regularly import useContext to access context value
   *
   * @returns Current store state
   */
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useStore = () => useContext(this.context);

  /**
   * A function that creates provider state for context provider value
   *
   * @param {{[string]:[any, function, object:{[string]: object | function }]}} combine Combined state object
   * @returns
   */
  createProviderState(combine) {
    // Get keys of the combine reducer
    const keys = Object.keys(combine);
    // switch reducer function for dispatch function using useReducer
    const providerState = keys.reduce((acc, key) => {
      const [initState, stateReducer, stateAction] = combine[key];
      const [state, dispatch] = useReducer(stateReducer, initState);
      return {
        ...acc,
        [key]: [state, dispatch, stateAction],
      };
    }, {});
    return providerState;
  }

  /**
   * A component that provides a global store to it's children component.
   *
   * @param {{}} param0 Nested children of component
   * @returns
   */
  Provider = ({ value, children }) => {
    const state = this.createProviderState(value);
    const StoreContext = this.context;
    return (
      <StoreContext.Provider value={state}>{children}</StoreContext.Provider>
    );
  };
}
