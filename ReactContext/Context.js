import React, { createContext, useContext } from 'react';

// Create context for storing app state
export const StoreContext = createContext({});
// Set context name
StoreContext.displayName = 'Store Name';

/**
 * A function that returns the store.
 *
 * Note: The function prevents the need to regularly import useContext to access context value
 *
 * @returns Current store state
 */
export const useStore = () => useContext(StoreContext);

/**
 * A component that provides a global store to it's children component.
 *
 * @param {*} param0 Nested children of component
 * @returns
 */
export const StoreProvider = ({ value, children }) => (
  <StoreContext.Provider value={value}>{children}</StoreContext.Provider>
);
