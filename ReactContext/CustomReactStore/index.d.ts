import React from 'react';
/**
 * State object
 */
export interface State {
    [key: string]: any;
}
/**
 * Action object
 */
export interface Action {
    type: string;
    [data: string]: any;
}
/**
 * Action objects / functions
 */
export interface Actions {
    [action: string]: Action | ((...args: any[]) => Action);
}
/**
 * Array of [State object, Reducer function, Actions object]
 */
export declare type CombineStoreArrType<S, A> = [S, React.Reducer<S, A>, A];
export interface CombineStoreType<S, A> {
    [name: string]: CombineStoreArrType<S, A>;
}
/**
 * Array of [State object, Dispatch function, Actions object]
 */
export declare type CombinedStoreArrType<S, A> = [S, React.Dispatch<A>, A];
export interface CombinedStoreType<S, A> {
    [name: string]: CombinedStoreArrType<S, A>;
}
/**
 * A class that implements and simplifies React Context
 */
export declare class ContextStore<S extends State> {
    context: React.Context<S | {}>;
    /**
     *
     * @param {string} displayName - Display name of the store for debugging
     * @param {S | {}} initValue - Value to initialize the store with
     */
    constructor(displayName?: string, initValue?: S | {});
    /**
     * A function that returns the store.
     *
     * Note: The function prevents the need to regularly import useContext to access context value
     *
     * @returns {{} | S} Current store state
     */
    useStore: () => {} | S;
    /**
     * A component that provides a global store to it's children component.
     *
     * @param {{value: any, children: React.ReactChildren}} param0 Nested children of component
     * @returns
     */
    Provider: ({ value, children }: {
        value: S;
        children: React.ReactNode;
    }) => React.FunctionComponentElement<React.ProviderProps<{} | S>>;
}
/**
 * A class that implements and simplifies React Context with Combine Reducer
 */
export declare class ReduxStore<S extends State, A extends Actions> {
    context: React.Context<CombinedStoreArrType<S, A> | {}>;
    /**
     *
     * @param {string} displayName - Display name of the store for debugging
     * @param {CombinedStoreArrType<S, A> | {}} initValue - Value to initialize the store with
     */
    constructor(displayName?: string, initValue?: CombinedStoreArrType<S, A> | {});
    /**
     * A function that returns the store.
     *
     * Note: The function prevents the need to regularly import useContext to access context value
     *
     * @returns {{} | CombinedStoreArrType<S, A>} Current store state
     */
    useStore: () => {} | CombinedStoreArrType<S, A>;
    /**
     * A component that provides a global store to it's children component.
     *
     * @param {{value: CombineStoreArrType<S, A>, children: React.ReactNode}} param0 Nested children of component
     * @returns
     */
    Provider: ({ value: [initState, stateReducer, stateAction], children, }: {
        value: CombineStoreArrType<S, A>;
        children: React.ReactNode;
    }) => React.FunctionComponentElement<React.ProviderProps<{} | CombinedStoreArrType<S, A>>>;
}
/**
 * A class that implements and simplifies React Context with Combine Reducer
 */
export declare class CombineReduxStore<S extends State, A extends Actions> {
    context: React.Context<CombinedStoreType<S, A> | {}>;
    /**
     *
     * @param {string} displayName - Display name of the store for debugging
     * @param {CombineStoreType<S, A> | {}} initValue - Value to initialize the store with
     */
    constructor(displayName?: string, initValue?: CombineStoreType<S, A> | {});
    /**
     * A function that returns the store.
     *
     * Note: The function prevents the need to regularly import useContext to access context value
     *
     * @returns {CombinedStoreType<S, A> | {}} Current store state
     */
    useStore: () => CombinedStoreType<S, A> | {};
    /**
     * A function that creates provider state for context provider value
     *
     * @param {CombinedStoreType<S, A>} combineState state object to be combined
     * @returns {CombinedStoreType<S, A>}
     */
    createProviderState: (combineState: CombineStoreType<S, A>) => CombinedStoreType<S, A>;
    /**
     * A component that provides a global store to it's children component.
     *
     * @param {{value: CombineStoreType<S, A>, children: React.ReactNode}} param0 Nested children of component
     * @returns
     */
    Provider: ({ value, children }: {
        value: CombineStoreType<S, A>;
        children: React.ReactNode;
    }) => React.FunctionComponentElement<React.ProviderProps<{} | CombinedStoreType<S, A>>>;
}
declare const AllStores: {
    ContextStore: typeof ContextStore;
    ReduxStore: typeof ReduxStore;
    CombineReduxStore: typeof CombineReduxStore;
};
export default AllStores;
