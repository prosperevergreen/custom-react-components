
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CombineReduxStore = exports.ReduxStore = exports.ContextStore = void 0;
const react_1 = __importDefault(require("react"));
/**
 * A class that implements and simplifies React Context
 */
class ContextStore {
    // Initialize the context
    context;
    /**
     *
     * @param {string} displayName - Display name of the store for debugging
     * @param {S | {}} initValue - Value to initialize the store with
     */
    constructor(displayName = 'React Context Store', initValue = {}) {
        this.context = react_1.default.createContext(initValue);
        this.context.displayName = displayName;
    }
    /**
     * A function that returns the store.
     *
     * Note: The function prevents the need to regularly import useContext to access context value
     *
     * @returns {{} | S} Current store state
     */
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useStore = () => react_1.default.useContext(this.context);
    /**
     * A component that provides a global store to it's children component.
     *
     * @param {{value: any, children: React.ReactChildren}} param0 Nested children of component
     * @returns
     */
    Provider = ({ value, children }) => {
        const StoreContext = this.context;
        return react_1.default.createElement(StoreContext.Provider, { value }, children);
    };
}
exports.ContextStore = ContextStore;
/**
 * A class that implements and simplifies React Context with Combine Reducer
 */
class ReduxStore {
    // Initialize the context
    context;
    /**
     *
     * @param {string} displayName - Display name of the store for debugging
     * @param {CombinedStoreArrType<S, A> | {}} initValue - Value to initialize the store with
     */
    constructor(displayName = 'React Redux Store', initValue = {}) {
        this.context = react_1.default.createContext(initValue);
        this.context.displayName = displayName;
    }
    /**
     * A function that returns the store.
     *
     * Note: The function prevents the need to regularly import useContext to access context value
     *
     * @returns {{} | CombinedStoreArrType<S, A>} Current store state
     */
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useStore = () => react_1.default.useContext(this.context);
    /**
     * A component that provides a global store to it's children component.
     *
     * @param {{value: CombineStoreArrType<S, A>, children: React.ReactNode}} param0 Nested children of component
     * @returns
     */
    Provider = ({ value: [initState, stateReducer, stateAction], children, }) => {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        const [state, dispatch] = react_1.default.useReducer(stateReducer, initState);
        const StoreContext = this.context;
        return react_1.default.createElement(StoreContext.Provider, { value: [state, dispatch, stateAction] }, children);
    };
}
exports.ReduxStore = ReduxStore;
/**
 * A class that implements and simplifies React Context with Combine Reducer
 */
class CombineReduxStore {
    // Initialize the context
    context;
    /**
     *
     * @param {string} displayName - Display name of the store for debugging
     * @param {CombineStoreType<S, A> | {}} initValue - Value to initialize the store with
     */
    constructor(displayName = 'Combine Redux Store', initValue = {}) {
        this.context = react_1.default.createContext(initValue);
        this.context.displayName = displayName;
    }
    /**
     * A function that returns the store.
     *
     * Note: The function prevents the need to regularly import useContext to access context value
     *
     * @returns {CombinedStoreType<S, A> | {}} Current store state
     */
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useStore = () => react_1.default.useContext(this.context);
    /**
     * A function that creates provider state for context provider value
     *
     * @param {CombinedStoreType<S, A>} combineState state object to be combined
     * @returns {CombinedStoreType<S, A>}
     */
    createProviderState = (combineState) => {
        // Get keys of the combine reducer
        const keys = Object.keys(combineState);
        // switch reducer function for dispatch function using useReducer
        const combinedState = keys.reduce((acc, key) => {
            const [initState, stateReducer, stateAction] = combineState[key];
            const [state, dispatch] = react_1.default.useReducer(stateReducer, initState);
            return {
                ...acc,
                [key]: [state, dispatch, stateAction],
            };
        }, {});
        return combinedState;
    };
    /**
     * A component that provides a global store to it's children component.
     *
     * @param {{value: CombineStoreType<S, A>, children: React.ReactNode}} param0 Nested children of component
     * @returns
     */
    Provider = ({ value, children }) => {
        const state = this.createProviderState(value);
        const StoreContext = this.context;
        return react_1.default.createElement(StoreContext.Provider, { value: state }, children);
    };
}
exports.CombineReduxStore = CombineReduxStore;
const AllStores = { ContextStore, ReduxStore, CombineReduxStore };
exports.default = AllStores;
