import React from 'react';

/**
 * State object
 */
export interface State {
  [key: string]: any;
}

/**
 * Action object
 */
export interface Action {
  type: string;
  [data: string]: any;
}

/**
 * Action objects / functions
 */
export interface Actions {
  [action: string]: Action | ((...args: any[]) => Action);
}

/**
 * Array of [State object, Reducer function, Actions object]
 */
export type CombineStoreArrType<S, A> = [S, React.Reducer<S, A>, A];

/**
 * Object with combine store data
 */
export interface CombineStoreType<S, A> {
  [name: string]: CombineStoreArrType<S, A>;
}

/**
 * Array of [State object, Dispatch function, Actions object]
 */
export type CombinedStoreArrType<S, A> = [S, React.Dispatch<A>, A];

/**
 * Object with combined store data
 */
export interface CombinedStoreType<S, A> {
  [name: string]: CombinedStoreArrType<S, A>;
}

/**
 * A class that implements and simplifies React Context
 */
export class ContextStore<S extends State> {
  // Initialize the context
  context: React.Context<S | {}>;

  /**
   *
   * @param {string} displayName - Display name of the store for debugging
   * @param {S | {}} initValue - Value to initialize the store with
   */
  constructor(
    displayName: string = 'React Context Store',
    initValue: S | {} = {}
  ) {
    this.context = React.createContext(initValue);
    this.context.displayName = displayName;
  }

  /**
   * A function that returns the store.
   *
   * Note: The function prevents the need to import useContext to access context value
   *
   * @returns {{} | S} Current store state
   */
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useStore = (): {} | S => React.useContext(this.context);

  /**
   * A component that provides a global store to it's children component.
   *
   * @param {{value: any, children: React.ReactChildren}} param0 Nested children of component
   * @returns
   */
  Provider = ({ value, children }: { value: S; children: React.ReactNode }) => {
    const StoreContext = this.context;
    return React.createElement(StoreContext.Provider, { value }, children);
  };
}

/**
 * A class that implements React Context with a redux-like interface
 */
export class ReduxStore<S extends State, A extends Actions> {
  // Initialize the context
  context: React.Context<CombinedStoreArrType<S, A> | {}>;

  /**
   *
   * @param {string} displayName - Display name of the store for debugging
   * @param {CombinedStoreArrType<S, A> | {}} initValue - Value to initialize the store with
   */
  constructor(
    displayName: string = 'React Redux Store',
    initValue: CombinedStoreArrType<S, A> | {} = {}
  ) {
    this.context = React.createContext(initValue);
    this.context.displayName = displayName;
  }

  /**
   * A function that returns the store.
   *
   * Note: The function prevents the need to import useContext to access context value
   *
   * @returns {{} | CombinedStoreArrType<S, A>} Current store state
   */
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useStore = (): {} | CombinedStoreArrType<S, A> => React.useContext(this.context);

  /**
   * A component that provides a global store to it's children component.
   *
   * @param {{value: CombineStoreArrType<S, A>, children: React.ReactNode}} param0 Nested children of component
   * @returns
   */
  Provider = ({
    value: [initState, stateReducer, stateAction],
    children,
  }: {
    value: CombineStoreArrType<S, A>;
    children: React.ReactNode;
  }) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [state, dispatch] = React.useReducer(stateReducer, initState);
    const StoreContext = this.context;
    return React.createElement(
      StoreContext.Provider,
      { value: [state, dispatch, stateAction] },
      children
    );
  };
}

/**
 * A class that implements React Context with a redux-like interface having the combine store functionality
 */
export class CombineReduxStore<S extends State, A extends Actions> {
  // Initialize the context
  context: React.Context<CombinedStoreType<S, A> | {}>;
  /**
   *
   * @param {string} displayName - Display name of the store for debugging
   * @param {CombineStoreType<S, A> | {}} initValue - Value to initialize the store with
   */
  constructor(
    displayName: string = 'Combine Redux Store',
    initValue: CombineStoreType<S, A> | {} = {}
  ) {
    this.context = React.createContext(initValue);
    this.context.displayName = displayName;
  }

  /**
   * A function that returns the store.
   *
   * Note: The function prevents the need to import useContext to access context value
   *
   * @returns {CombinedStoreType<S, A> | {}} Current store state
   */
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useStore = (): CombinedStoreType<S, A> | {} => React.useContext(this.context);

  /**
   * A function that creates provider state for context provider value
   *
   * @param {CombinedStoreType<S, A>} combineState state object to be combined
   * @returns {CombinedStoreType<S, A>}
   */
  createProviderState = (combineState: CombineStoreType<S, A>): CombinedStoreType<S, A> => {
    // Get keys of the combine reducer
    const keys = Object.keys(combineState);
    // switch reducer function for dispatch function using useReducer
    const combinedState = keys.reduce((acc, key) => {
      const [initState, stateReducer, stateAction] = combineState[key];
      const [state, dispatch] = React.useReducer(stateReducer, initState);
      return {
        ...acc,
        [key]: [state, dispatch, stateAction],
      };
    }, {});
    return combinedState;
  };

  /**
   * A component that provides a global store to it's children component.
   *
   * @param {{value: CombineStoreType<S, A>, children: React.ReactNode}} param0 Nested children of component
   * @returns
   */
  Provider = ({ value, children }: { value: CombineStoreType<S, A>; children: React.ReactNode; }) => {
    const state = this.createProviderState(value);
    const StoreContext = this.context;
    return React.createElement(
      StoreContext.Provider,
      { value: state },
      children
    );
  };
}

const AllStores = { ContextStore, ReduxStore, CombineReduxStore };

export default AllStores;
